#this script allows to clone part of the TTree. More specifically, the scripts loop over the .root files in a given folder and generates 
#a set of new files with the slimmed ntuples
# 
# Example: python slimFiles.py -s input_path -o output_path -i branches_to_include.txt -t "tree_name"
#
# the txt should contains the list of the branches to keep organized in the following way: 1 branch-name/line with no qutes. 
#Example of txt file follows:
# dilep_*
# taus_*
# event_number

import re
import sys
import string
import os
import shutil
import subprocess

from os import listdir
from argparse import ArgumentParser

import ROOT



parser = ArgumentParser()
parser.add_argument("-s", "--source-dir", dest="sourceDir",
                    help="Directory that contains the .root files you want to slim", metavar="SOURCE_DIR")
parser.add_argument("-o", "--output-dir", dest="outputDir",
                    help="Directory where will be created the slimmed files", metavar="OUTPUT_DIR")
parser.add_argument("-i", "--branches-to-include-list", dest="branchList", 
                    help="file containg the list of the branches to include", metavar="BRANCH_LIST")
parser.add_argument("-t", "--tree-name", dest="treeName", 
                    help="name of the TTree in the .root files", metavar="TREE_NAME")
parser.add_argument("-q", "--quiet",
                    action="store_false", dest="verbose", default=True,
                    help="don't print status messages to stdout")

args = parser.parse_args()

# create the string with all the branches to include
file_branch = open(args.branchList, "r")

branches_to_include = ""
isFirst = True

for line in file_branch:
    for t in line.split():
        if (isFirst):
            branches_to_include += "\""+t+"\""
            isFirst = False
        else:
            branches_to_include += ",\""+t+"\""
            

print branches_to_include

input_path  = args.sourceDir
output_path = args.outputDir

for file in listdir(input_path):
    if (file.endswith(".root") == False): continue
    root_file      = input_path + "/" + file + ":"+args.treeName
    new_root_file  = output_path + "/" + "slimmed_" + file   
    string_command = "rootslimtree  -i " + branches_to_include + " -e \"*\" " + str(root_file) + " "+ str(new_root_file)
#    string_command = "\""+string_command+"\""
    print string_command
    subprocess.call(string_command, shell=True)
#    Rootslimtree  -i  branches_to_include   -e "*"   root_file new_root_file
#    subprocess.call(["rootslimtree " ,"-i",str(branches_to_include),"-e","*",str(root_file),str(new_root_file)])
